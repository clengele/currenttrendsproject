import subprocess
for input_size in [20,40,100]:
    for model in [1,2,3]:
        for inversed in [0,1]:
            bashCmd = ["sbatch", "run_job.sh","tweet_generatorv3.py",input_size,model,inversed]
            process = subprocess.Popen(bashCmd)
