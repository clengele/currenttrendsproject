#!/usr/bin/env python
# coding: utf-8

import numpy as np
import pandas as pd
import re
import sys
import unicodedata
from nltk.corpus import stopwords
from keras.callbacks import LambdaCallback
from keras.models import Sequential
from keras.layers import Dense, Dropout, LSTM
from keras.utils import np_utils
from keras.callbacks import ModelCheckpoint

df = pd.read_excel('EMtweet.xlsx')
fr_df = df[df["Language"] == "fr"] # Keep only french tweets
tweets_macron = fr_df[["Text"]] # Keep only text information (the tweet itself)

# Filter RT
#  Hypothesis : the retweeted tweets are in the same writing style as Macron then we keep them
list_tweet = []
for tweet in tweets_macron["Text"]:
    if tweet[0:2] == "RT":
       list_tweet.append(":".join(tweet.split(":")[1::]))
    else:
        list_tweet.append(tweet)

def remove_accents(input_str):
    nfkd_form = unicodedata.normalize('NFKD', input_str)
    return u"".join([c for c in nfkd_form if not unicodedata.combining(c)])

# Filter hyperlinks
for index, tweet in enumerate(list_tweet):
        list_tweet[index]=re.sub(r'https?:\/\/.*', '', tweet, flags=re.MULTILINE)
  

# remove Caps and accents
for index, tweet in enumerate(list_tweet):
    list_tweet[index] = list_tweet[index].lower()
    list_tweet[index] = remove_accents(list_tweet[index]) # replace accented caracter by there non accented counterpart


# Elimination of \n 
for index in range(len(list_tweet)):
    list_tweet[index]  = list_tweet[index].replace("\n", '')
    list_tweet[index]  = list_tweet[index].replace("\"", '')
    list_tweet[index]  = list_tweet[index].replace("&", '')
    list_tweet[index]  = list_tweet[index].replace("*", '')
    list_tweet[index]  = list_tweet[index].replace("%", '')
    list_tweet[index]  = list_tweet[index].replace("(", '')
    list_tweet[index]  = list_tweet[index].replace(")", '')
    list_tweet[index]  = list_tweet[index].replace("+", '')
    list_tweet[index]  = list_tweet[index].replace("/", '')
    list_tweet[index]  = list_tweet[index].replace(";", '')
    list_tweet[index]  = list_tweet[index].replace("?", '')
    list_tweet[index]  = list_tweet[index].replace("[", '')
    list_tweet[index]  = list_tweet[index].replace("]", '')
    list_tweet[index]  = list_tweet[index].replace("_", '')


text = " ".join(list_tweet)
used_char = sorted(list(set(text)))
char_indices = dict((c, i) for i, c in enumerate(used_char))
indices_char = dict((i, c) for i, c in enumerate(used_char))

for c in used_char[-59:]:
    for index, tweet in enumerate(list_tweet):
        list_tweet[index] = tweet.replace(c,'')
used_char = used_char[:-59]        
list_tweet[1]

char_indices = dict((c, i) for i, c in enumerate(used_char))
indices_char = dict((i, c) for i, c in enumerate(used_char))
text = " ".join(list_tweet)

input_size = int(sys.argv[1]) # 40 #20
nb_of_chars = len(used_char)

x = []
y = []




# cut the text in semi-redundant sequences of maxlen characters
step = 1
sentences = []
next_chars = []
for tweet in list_tweet:
    for i in range(0, len(tweet) - input_size, step):
        in_seq = tweet[i: i + input_size] # input (len = input_size * size of a char)
        out_seq = tweet[i + input_size] # output (len = size of a char)
        x.append([char_indices[char] for char in in_seq]) # input processed for NN
        y.append(char_indices[out_seq]) # output processed for NN
        
x = np.reshape(x, (len(x), input_size, 1))
x = x / float(nb_of_chars)
y = np_utils.to_categorical(y)

def create_model_1(x,y,used_char):
    model = Sequential()
    model.add(LSTM(128,  input_shape=(x.shape[1], x.shape[2])))
    model.add(Dense(len(used_char), activation='softmax'))
    # optimizer = RMSprop(lr=0.01)
    model.compile(loss='categorical_crossentropy', optimizer='adam')
    return model

def create_model_2(x,y):
    model = Sequential()
    model.add(LSTM(256, input_shape=(x.shape[1], x.shape[2]), return_sequences=True))
    model.add(Dropout(0.2))
    model.add(LSTM(256, return_sequences=True))
    model.add(Dropout(0.2))
    model.add(LSTM(128))
    model.add(Dropout(0.2))
    model.add(Dense(y.shape[1], activation='softmax'))

    model.compile(loss='categorical_crossentropy', optimizer='adam')
    return model

if int(sys.argv[2]) == 1:
    model = create_model_1(x,y,used_char)
    print("model 1")
else:
    model = create_model_2(x,y)    
    print("model 1")

def sample(preds, temperature=1.0):
    # helper function to sample an index from a probability array
    preds = np.asarray(preds).astype('float64')
    preds = np.log(preds) / temperature
    exp_preds = np.exp(preds)
    preds = exp_preds / np.sum(exp_preds)
    probas = np.random.multinomial(1, preds, 1)
    return np.argmax(probas)
file_results="./results_"+sys.argv[1]+"_"+sys.argv[2]+".txt"
f = open(file_results, "w+")
f.close()

def on_epoch_end(epoch, _):
    # Function invoked at end of each epoch. Prints generated text.
    f2 = open(file_results, "a")
    f2.write('----- Generating text after Epoch: %d -----\n' % epoch)
    
    tweet = np.random.choice(list_tweet) # select random tweet
    
    f2.write('----- Generating with seed: "' + tweet + '"\n')
    for diversity in [0.2, 0.5, 1.0, 1.2]:

        generated = ''
        sentence = tweet[0: input_size]
        generated += sentence
        f2.write(generated)

        for i in range(120):
            x_pred = np.zeros((1, input_size, 1))
            for t, char in enumerate(sentence):
                x_pred[0, t, 0] = char_indices[char]
            
            preds = model.predict(x_pred, verbose=0)[0]
            next_index = sample(preds, diversity)
            next_char = indices_char[next_index]

            generated += next_char
            sentence = sentence[1:] + next_char

            f2.write(next_char)
        f2.write('\n----- End of Callback of Epoch %d -----\n' % epoch)
    f2.close()


#model.fit(x, y, epochs=4, batch_size=256, callbacks=LambdaCallback(on_epoch_end=on_epoch_end))
model.fit(x, y, epochs=100, batch_size=256, callbacks=LambdaCallback(on_epoch_end=on_epoch_end),verbose=0)

file_weights="./results_"+sys.argv[1]+"_"+sys.argv[2]+".h5"
model.save_weights(file_weights)
